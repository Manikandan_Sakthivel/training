package controlFlow;
/*Requirement:
    To print fibonocci using for loop, while loop and recursion

Entities:
    Fibonacci

Method Signature:
	public static void main(String[] args)

Jobs To Be Done:
  1)The series length input is initialized as static type.
  2)For each number
   		2.1) the next number is added to the previous number and assigned to the new variable.
   		2.2)The next number becomes previous number and the new variable value becomes next number.
  3)While each condition is true
  		3.1)The next number is added to the previous number and assigned to the new variable.
   		3.2)The next number becomes previous number and the new variable value becomes next number.
   		3.3)The condition variable is incremented.
  4)In recursion method , The series length variable is declared as condition variable as it passed as individual variable.The process continuous until it becomes 	false. 
  		4.1)Checks whether the input is zero or one.
  			4.1.1)If zero , it returns 0
  			4.1.2)If one , it returns 1
  		4.2)Otherwise it adds the fibonacci recursion of two previous number and a previous number of the condition variable and 			return the value.
*/
//OUTPUT:
//For Loop:
public class Fibonacci {
	static int previousNumber = 0;
    static int nextNumber = 1;
	public static void ForLoop() {
		for (int i = 1; i <= 10; ++i)
        {
            
            System.out.print(previousNumber+" ");  //0 1 1 2 3 5 8 13 21 34
            int sum = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = sum;
        }
	}
	
	public static void WhileLoop() {
		int i=1;
		while(i <= 10)
        {
            System.out.println("WHILE LOOP");
            System.out.print(previousNumber+" ");  //0 1 1 2 3 5 8 13 21 34
            int sum = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = sum;
            i++;
        }
	}
	
	public static int fibonacciRecursion(int n){
	    if(n == 0){
	        return 0;
	    }
	    if(n == 1 || n == 2){
	        return 1;
	    }
	    return fibonacciRecursion(n-2) + fibonacciRecursion(n-1);
	    }
	
    public static void main(String[] args) 
    {
       
    	System.out.println("FOR LOOP");
    	ForLoop();
    	
    	System.out.println("WHILE LOOP");
    	WhileLoop();
    	
    	for(int i = 0; i < 10; i++){
            System.out.println("RECURSION");
            System.out.print(fibonacciRecursion(i) +" ");  //0 1 1 2 3 5 8 13 21 34
            }
    }

	
}

