package operators;
/*
Requirement:
    To explain the reason for the value '6' is repeated twice in a row.
    class PrePostDemo {
        public static void main(String[] args){
            int i = 3;
            i++;
            System.out.println(i);    // "4"
            ++i;
            System.out.println(i);    // "5"
            System.out.println(++i);  // "6"
            System.out.println(i++);  // "6"
            System.out.println(i);    // "7"
        }
    }
Entities:
    PrePostDemo 
    
Method Signature:
	public static void main(String[] args)
    
Jobs to be done:
    1)Some value is initialized to the integer variable.
    2)By referring the question.
    	2.1)the value is incremented by one and printed.
    	2.2)Again the value is incremented and printed.
    	2.3)Again the value is incremented and printed.
    	2.4)Now the Value is printed and incremented.

Solution:
	++i increments the value of i by 1 and returns the i value.
	i++ returns the value of i and increments the value by 1.
*/
class PrePostDemo {
    public static void main(String[] args){
        int i = 3;
        i++;
        System.out.println(i);    // "4"
        ++i;
        System.out.println(i);    // "5"
        System.out.println(++i);  // "6" the i value is incremented from 5 to 6 and printed.
        System.out.println(i++);  // "6"  the i value is printed and got incremented from 6 to 7
        System.out.println(i);    // "7"
    }
}