package operators;
/*
Requirement:
    To change the given progarm in the form of compound assignments

Entities:
    ArithmeticDemo

Method Signature:
	public static void main(String[] args)
	
Jobs to be done:
    1)The compound assignment is reducing the arithmetic operation length.
    2)some value is assigned to the integer variable.
    3)Now the value is carried down to various arithmetic operation under compound assignments.
*/
 class ArithmeticDemo {

    public static void main (String[] args){
        int result = 1 + 2; // result is now 3
        System.out.println(result);

        result -= 1; // result is now 2
        System.out.println(result);

        result *= 2; // result is now 4
        System.out.println(result);

        result /= 2; // result is now 2
        System.out.println(result);

        result += 8; // result is now 10
        System.out.println(result);
        result %= 7; // result is now 3
        System.out.println(result);

    }
}