/*
 * Requirements:
 * To Demonstrate the catching multiple exceptions.
 * 
 * Entities:
 * public class ExceptionDemo
 * 
 * Function Declaration:
 * public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * Create the package exceptionHandling.
 * Import the Scanner.
 * Create the class ExceptionDemo
 * Under try block number1 and number2 are inputs,then the number is number1 divided by number2 and also String is declared as null value, print the string length.
 * If the Zero division error occurs in the try block catch the Arithmetic Exception part, print the exception statement or else print the null pointer exception statement,because the String value is declared as null.
 */
package com.kpr.training.exceptionHandling;

import java.util.Scanner;

public class ExceptionDemo {
	
	public static void main(String[] args) {
		
		try {
			Scanner sc = new Scanner(System.in);
			System.out.println("enter numbers");
			int number1 = sc.nextInt();
			int number2 = sc.nextInt();
			int number = number1/number2;
			String string = null;
			System.out.println(string.length());
		} catch (ArithmeticException exception)
		{
			System.out.println("Arithmetic Exception occurs");
		} catch (NullPointerException exception)
		{
			System.out.println("Null Pointer exception occurs");
		
		}
		
	}

}