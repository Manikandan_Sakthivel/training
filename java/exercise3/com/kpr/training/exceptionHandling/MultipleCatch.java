/*
 * Requirements:
 *    Mutiple catch block 
      a]Explain with Example
 *      
 * Entities:
 *    MulipleCatch
 *    
 * Function Declaration:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1.Create the package exception_handling.
 *    2.Import the Scanner.
 *    3.Create the class ExceptionDemo
 *    4.try and catch blocks are created.
 *    5.Prints the particular exception statement part.
 
 * Explanation:   
 *    1.Under try block number1 and number2 are inputs,then the number3 is number1 divided by number2 and also String is declared as
 *    null value, print the string length.
 *    2.Under try block, if the Zero division error occurs throws the catch block Arithmetic Exception part, print the exception 
 *    statement or else print the null pointer exception statement,because the String value is declared as null.
 */
package com.kpr.training.exceptionHandling;

import java.util.Scanner;

public class MultipleCatch {
	
	public static void main(String[] args) {
		try {
			Scanner scanner = new Scanner(System.in);
			System.out.println("�nter numbers");
			int number1 = scanner.nextInt();
			int number2 = scanner.nextInt();
			int number3 = number1 / number2;
			String string = null;
			System.out.println(string.length());
		} catch (ArithmeticException exception) {
			System.out.println("Arithmetic Exception occurs");
		} catch (NullPointerException exception) {
			System.out.println("NullPointer Exception occurs");
		}
	}
	
}