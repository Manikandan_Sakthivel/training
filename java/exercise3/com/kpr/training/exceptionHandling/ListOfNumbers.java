/*
 * Requirements:
 * Write a program ListOfNumbers using try and catch block.
 * 
 * Entities:
 * public class ListOfNumbers 
 * 
 * Function Declaration:
 * public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * Create the package exceptionHandling
 * Create the class ListOfNumbers and create the object listOfNumbers
 * Declare the length of the array.
 * Under try block the value as assigned to the value of the index.
 * If the value is assigned to greater than length of the array(out of index range) throws the catch block prints the IndexOutOfBoundsException statement.
 * If the value is not in a number format throws the catch block prints the NumberFormatException statement.
 */
package com.kpr.training.exceptionHandling;

public class ListOfNumbers {

	public static void main(String[] args) {
		
		int[] arrayOfNumbers = new int[10];
		
		try {
			arrayOfNumbers[10] = 16;
		} catch (NumberFormatException exception) {
			System.out.println("Number Format Exception " + exception.getMessage());
	    } catch (IndexOutOfBoundsException exception) {
	    	System.out.println("Index Out Of Bounds Exception " + exception.getMessage());
	    }

	}

}