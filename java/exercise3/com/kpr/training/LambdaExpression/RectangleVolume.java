/*
 * Requirements : 
 * 		Write a program to print the volume of  a Rectangle using lambda expression
 * Entities :
 * 		interface AreaOfRectangle,
 * 		public class RectangleVolume.
 * Function Declaration :
 * 		double area(double num1, double num2),
 * 		public static void main(String[] args).
 * Jobs To Be Done:
 * 		1)Importing the FunctionalInterface and Scanner.
 * 		2)Creating a AreaOfRectangle interface and create a area method.
 * 		3)Creating a Rectanglevolume class.
 * 		4)Creating main method.
 * 		5)Creating rectangleArea reference and defining a lambda function.
 * 		6)Printing the result using area method.
 */
package com.kpr.training.LambdaExpression;

import java.util.Scanner;
import java.lang.FunctionalInterface;

@FunctionalInterface
interface AreaOfRectangle {
	
	double area(double num1, double num2);
}

public class RectangleVolume {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double length = scanner.nextDouble();
		double breadth = scanner.nextDouble();
		AreaOfRectangle rectangleArea = (num1, num2) -> num1 * num2;
		System.out.println(rectangleArea.area(length, breadth));

	}
}