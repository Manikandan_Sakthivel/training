/*
 * Requirements : 
 * 		Convert this following code into a simple lambda expression
        int getValue(){
            return 5;
        }
 * Entities :
 * 		interface Value,
 * 		public class ConvertCode.
 * Function Declaration :
 * 		int getValue();
 * 		public static void main(String[] args).
 * Jobs To Be Done:
 * 		1)Importing the FunctionalInterface.
 * 		2)Creating a Value interface and create a getValue method.
 * 		3)Creating a ConvertCode class.
 * 		4)Creating main method.
 * 		5)Creating test reference and defining a lambda function to return 5.
 * 		6)Printing the result using getValue method.
 */
package com.kpr.training.LambdaExpression;

import java.lang.FunctionalInterface;

@FunctionalInterface
interface Value {
	
	int getValue();
}

public class ConvertCode {

	public static void main(String[] args) {
		Value test = () -> 5;
		System.out.println(test.getValue());
		

	}

}