/*
 * Requirements : 
 * 		 write a program to print difference of two numbers using lambda expression 
 *  and the single method interface
 * Entities :
 * 		interface MyInterface,
 * 		public Difference.
 * Function Declaration :
 * 		int difference(int one, int two),
 * 		public static void main(String[] args).
 * Jobs To Be Done:
 * 		1)Importing the FunctionalInterface.
 * 		2)Creating a Diff interface and create a difference method.
 * 		3)Creating a Difference class.
 * 		4)Creating main method.
 * 		5)Creating test reference and defining a lambda function.
 * 		6)Printing the result using difference method.
 */
package com.kpr.training.LambdaExpression;

import java.lang.FunctionalInterface;
import java.util.Scanner;

@FunctionalInterface
interface Diff {

	int difference(int one, int two);
}
public class Difference {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int number1 = scanner.nextInt();
		int number2 = scanner.nextInt();
		Diff test = (num1, num2) -> num1 - num2;
		System.out.println(test.difference(number1, number2));
	}

}