/*
 * Requirement: write a program that allows to write data to multiple files together using byte
 * array outputstream .
 * 
 * Entity: MultipleFile
 * 
 * Function Signature : public static void main(String[] args)
 */

package com.kpr.training.IO_stream;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MultipleFile {
    public static void main(String[] args) throws IOException {
        String data = "ByteArrayOutputStream of the Java IO API "
                	+ "enables you to capture data written to a stream in a byte array";
        File file1 = new File("D:\\file1.txt");
        File file2 = new File("D:\\file2.txt");
        
        ByteArrayOutputStream buf1 = new ByteArrayOutputStream();
        ByteArrayOutputStream buf2 = new ByteArrayOutputStream();

        byte[] array = data.getBytes();
        buf1.write(array);
        buf2.write(array);			

        
    }
}