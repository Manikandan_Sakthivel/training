/*
 * Requirements : 
 * 		To explain about contains(), subList(), retainAll() and give example.
 * 
 * Entities :
 * 		public class ListSample.
 * 
 * Function Declaration :
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 *      1) Create a ArrayList.
 *          1.1)Declare the object name as list
 *          1.2)Add the elements to the ArrayList of type Integer .
 *      2) Check a specific element is present in the list or not .
 *      3) Print the range of the element in the list.
 *      4) Create a new list .
 *          4.1)Declare a object as newlist
 *          4.2)Add the input to the newlist of type Integer .
 *      5) Print the element in new list that are also in the list.
 *      
 * Pseudo Code:
 *
 *public class ListSample{
 *     public static void main(String[] args) {
           ArrayList<Integer> list = new ArrayList<>();
           //Add the elements to the list
           System.out.println(list.contains(element));
           System.out.println(list.subList(start , end));
           ArrayList<Integer> newList = new ArrayList<>();
           //Add elements to newlist
           System.out.println("Before using retainall method : " + newList);
           newList.retainAll(list);
           System.out.println("After using retainall method : " + newList);
    }   

}
 *
 * 	    
 */
/*
 * Explanation:
 * 	contains():
 * 		returns true if the element is present in the list else returns false.
 * 	subList(starting index, ending index):
 * 		returns the list elements from starting index to ending index.
 * 	retainAll():
 * 		It is used to remove all the array list's elements that are not contained in the specified list.
 */
package com.kpr.training.list_and_set;


import java.util.ArrayList;

public class ListExample {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(100);
		list.add(200);
		list.add(300);
		list.add(400);
		list.add(500);
		list.add(600);
		System.out.println(list.contains(500)); // it prints true
		System.out.println(list.subList(1, 4)); //prints elements from index 1 to 3
		
		ArrayList<Integer> newList = new ArrayList<>();
		newList.add(400);
		newList.add(300);
		newList.add(1000);
		System.out.println("Before using retainall method : " + newList);
		newList.retainAll(list);
		System.out.println("After using retainall method : " + newList);
	}

}