/*
 * Requirements : 
 *   To write a program to demonstrate generics - for loop for list.
 *
 * Entities :
 *    public class ListDemo 
 *    
 * Function Declaration :
 *    	public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *     1)Creating the class ListDemo and list as names
 *     2)Adding elements to the list with add method 
 *     3)Iterating the list with iterator method and for loop
 */
package com.kpr.training.exercise3.generics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ListDemo {

    public static void main(String[] args) {
        ArrayList<String> names = new ArrayList<String>();
        String firstName = "Ashok";
        names.add(firstName);
        names.add("Arya");
        names.add("Surya");
        names.add("Ajith");
        names.add("Ajith");
        System.out.println(names.get(1));
        System.out.println("Iterating String list using while loop");
        Iterator<String> stringIterator = names.iterator();
        while (stringIterator.hasNext()) {
            System.out.println(stringIterator.next());
        }

        System.out.println("Iterating String list using for loop");
        for (String name : names) {
            System.out.println(name);
        }

        ArrayList<Integer> ages = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        for (String name : names) {
            ages.add(scanner.nextInt());
        }

        System.out.println("age at index 3 is : " + ages.get(3));
        System.out.println("Iterating Integer list using while loop");
        Iterator<Integer> intIterator = ages.iterator();
        while (intIterator.hasNext()) {
            System.out.println(intIterator.next());
        }

        System.out.println("Iterating Integer list using for loop");
        for (int age : ages) {
            System.out.println(age);
        }
    }
}
