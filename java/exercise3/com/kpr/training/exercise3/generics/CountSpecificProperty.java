/*
 * Requirements : 
 * 		Write a generic method to count the number of elements in a collection that have a specific
 * property (for example, odd integers, prime numbers, palindromes).
 *
 * Entities :
 * 		public class CountSpecificProperty.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1)Importing the ArrayList.
 * 		2)Creating the CountSpecificProperty class
 * 		3)Creating the count method which returns the count of odd numbers present in a list.
 * 		4)Creating the main method and create a list reference.
 * 		5)Adding elements inside a list.
 * 		6)Calling a count method and printing the number of odd numbers.
 */
package com.kpr.training.exercise3.generics;

import java.util.ArrayList;

public class CountSpecificProperty {
	
	public static int count(ArrayList<Integer> list) {
		int c = 0;
		for(int elements : list) {
			if(elements % 2 != 0) {
				c++;
			}
		}
		return c;
	}

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		list.add(4);
		list.add(3);
		list.add(2);
		list.add(1);
		System.out.println("Number of odd integers : " + count(list));

	}

}