/*
 * Requirements : 
 * 		What will be the output of the following program?

		public class UseGenerics {
    		public static void main(String args[]){  
        		MyGen<Integer> m = new MyGen<Integer>();  
        		m.set("merit");
        		System.out.println(m.get());
    		}
		}
		class MyGen<T> {
    		T var;
    		void  set(T var) {
        		this.var = var;
    		}
    		T get() {
        		return var;
    		}
		}

 * Entities :
 * 		public class UseGenerics.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1)Importing the ArrayList.
 * 		2)Creating the CountSpecificProperty class
 * 		3)Creating the count method which returns the count of odd numbers present in a list.
 * 		4)Creating the main method and create a list reference.
 * 		5)Adding elements inside a list.
 * 		6)Calling a count method and printing the number of odd numbers.
 */
package com.kpr.training.exercise3.generics;

public class UseGenerics {
	
    public static void main(String args[]){  
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(String string)
    {
        this.var = (T) string;
    }
    T get()
    {
        return var;
    }
}
/*
 * It gives a compile time error because while creating the reference the generic type is given as Integer,
 * but String is passed as argument in set method.
 */