/*Requirement:
 *      For the following code use the split method() and print in sentenceString website = "https-www-google-com";
 *      
 *Entity:
 *      SplitMethod
 *      
 *Function Signature:
 *      public static void main(String args[])
 *      
 *Jobs to be Done:
 *      1)Create a variable website of type String and assign the input .
 *      2)Create a array of type String.
 *          2.1)split the String using split method .
 *      3)Print the array using Iteration.      
 *      
 *Pseudo code:
 *class SplitMethod {

    public static void main(String args[]) {
        String website = "https-www-google-com";
        String[] array = website.split("-");

        for (String string : array) {
            System.out.println(string);
        }
    }
  }     
 * 
 */
package com.kpr.training.regex;

public class SplitMethod {

    public static void main(String args[]) {
        String website = "https-www-google-com";
        String[] array = website.split("-");

        for (String string : array) {
            System.out.println(string);
        }
    }
}