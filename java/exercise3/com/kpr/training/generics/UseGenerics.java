/*
 * Requirements : 
 * 		What will be the output of the following program?

		public class UseGenerics {
    		public static void main(String[] args){  
        		MyGen<Integer> m = new MyGen<Integer>();  
        		m.set("merit");
        		System.out.println(m.get());
    		}
		}
		class MyGen<T> {
    		T var;
    		void  set(T var) {
        		this.var = var;
    		}
    		T get() {
        		return var;
    		}
		}

 * Entities :
 * 		UseGenerics
 * 		MyGen<T>
 * Method Signature :
 * 		public static void main(String[] args)
 * 		void set(T var)
 * 		T get()
 * Jobs To Be Done:
 * 		1)Writing the program which is given in the requirements.
 * 		2)Printing the output of the given code.
 * 
 */
package com.kpr.training.generics;

public class UseGenerics {
	
    public static void main(String args[]){  
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(String string)
    {
        this.var = (T) string;
    }
    T get()
    {
        return var;
    }
}
/*
 * It gives a compile time error because while creating the reference the generic type is given as Integer,
 * but String is passed as argument in set method.
 */