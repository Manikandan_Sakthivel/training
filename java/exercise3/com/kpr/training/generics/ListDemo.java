/*
 * Requirements : 
 *   To write a program to demonstrate generics - for loop for list.
 *
 * Entities :
 *    ListDemo 
 *    
 * Method Signature :
 *    	public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1.Create a ArrayList, the arguments type as String.
 *        1.1 Declare the object as names.
 *        1.2 Declare the firstName as String type.
 *        1.3 Add firstName in the ArrayList.
 *        1.1 Add other names in the ArrayList Which type as String.
 *    2.Print the value of particular index.
 *    3.Print the ArrayList elements using iterator method with use of while loop.
 *    4.Print the ArrayList elements using foreach loop.
 *    5.Create another ArrayList, the argument type as Integer.
 *    	  5.1 Declare the object as ages.
 *    6. Get the input from the User.
 *    7. Add the ages value to the Second ArrayList using foreach loop.
 *    8. Print the value of particular index in the Second ArrayList.
 *    9. Print the Second ArrayList elements using iterator method with use of while loop.
 *   10. Print the Second ArrayList elements using foreach loop.
 *      
 * Pseudo Code:
 * Class ListDemo {
 * 
 * 		public static void main(String[] args) {
 *      	ArrayList<String> names = new ArrayList<String>();
 *      	// Declare firstName as string 
 *      	// Add the firstName and other elements to the ArrayList
 *      
 *      	System.out.println(names.get(index));
 *      
 *      	//using iterator
 *      	Iterator<String> stringIterator = names.iterator();
 *			while (stringIterator.hasNext()) {
 *			System.out.println(stringIterator.next());
 *			}
 *
 *      	//using foreach loop
 *			System.out.println("Iterating String list using for loop");
 *			for (String name : names) {
 *				System.out.println(name);
 *      	}
 *      
 *      	ArrayList<Integer> ages = new ArrayList<>();
 *      	Scanner scanner = new Scanner(System.in);
 *      	//Get the input from the User
 *      
 *      	for (String name : names) {
 *				ages.add(scanner.nextInt());
 *			}
 *		
 *			// Get particular index value
 *			System.out.println("age at particular index is : " + ages.get(index));
 *		
 *			// Using iterator
 *			System.out.println("Iterating Integer list using while loop");
 *			Iterator<Integer> intIterator = ages.iterator();
 *			while (intIterator.hasNext()) {
 *				System.out.println(intIterator.next());
 *			}
 *		
 *      	// using foreach loop
 *			System.out.println("Iterating Integer list using for loop");
 *			for (int age : ages) {
 *				System.out.println(age);
 *			}
 *	  	}
 * }
 *
 *            
 */
package com.kpr.training.generics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ListDemo {

	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<String>();
		String firstName = "Boobalan";
		names.add(firstName);
		names.add("Arya");
		names.add("Surya");
		names.add("Ajith");
		names.add("Ajith");
		System.out.println(names.get(1));
		System.out.println("Iterating String list using while loop");
		Iterator<String> stringIterator = names.iterator();
		while (stringIterator.hasNext()) {
			System.out.println(stringIterator.next());
		}

		System.out.println("Iterating String list using for loop");
		for (String name : names) {
			System.out.println(name);
		}

		ArrayList<Integer> ages = new ArrayList<>();
		Scanner scanner = new Scanner(System.in);
		for (String name : names) {
			ages.add(scanner.nextInt());
		}

		System.out.println("age at index 3 is : " + ages.get(3));
		System.out.println("Iterating Integer list using while loop");
		Iterator<Integer> intIterator = ages.iterator();
		while (intIterator.hasNext()) {
			System.out.println(intIterator.next());
		}

		System.out.println("Iterating Integer list using for loop");
		for (int age : ages) {
			System.out.println(age);
		}
	}
}