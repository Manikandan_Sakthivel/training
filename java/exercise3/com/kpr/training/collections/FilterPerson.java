/*
 * Requirement:
 * 		To filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

	Entity:
		FilterPerson
	
	Function Declaration:
		public static void main(String[] args)
		
	Jobs To Be Done:
		1)Importing the necessary packages in Collection Package.
		2)Creating the class name called FilterPerson
		3)Creating the list name as roster and the values are predefined in Person.java file
		4)Creating a arraylist of name as name
		5)Checking the condition if the person is male, If YES, he is added to the name list.
		6)Printing the first filtered person and last filtered person.
		7)To print the random filtered person,Random class with rand object is declared.
		8)Finding the index of the random value.
		9)Printing the Random Filtered Name.  
 */

package com.kpr.training.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FilterPerson {
	
	public static void main(String[] args) {
		
		List<Person> roster = Person.createRoster();
		ArrayList<String> name = new ArrayList<>();

		for (Person p : roster) {
			
			if (p.getGender() == Person.Sex.MALE) {

				name.add(p.getName());

			}
		}
		int size = name.size();
		System.out.println("The First Filtered Person is " + name.get(0));
		System.out.println("The Last Filtered Person is " + name.get(size-1));
		
		Random rand = new Random();
		int index = rand.nextInt(size);
		System.out.println("The Random Filtered Person is " + name.get(index));
		
	}
}
