/*
 * Requirement:
 *      Addition,Substraction,Multiplication and Division concepts are achieved using Lambda expression
 *  and functional interface
 *  Entity:
 *      LambdaExample
 *      Arithmetic
 *  Function declartion:
 *      int operation(int a, int b)
 *  jobs to done:
 *      1)Create package named as collections.
 *      2)Create interface under the main method declare the method operation.
 *      3)create class LambdaExample and create main method in it.
 *      4)Under the main method define the methods which is declared in interface as Addition,Subtracrion,Multiplication and Divison.
 *      5) Print the results as per the function declared.
 */
package com.kpr.training.collections;

interface Arithmetic {
    int operation(int a, int b);
}


public class LambdaExample {
	
    public static void main(String[] args) {

        Arithmetic addition = (int a, int b) -> (a + b);
        System.out.println("Addition = " + addition.operation(5, 6));

        Arithmetic subtraction = (int a, int b) -> (a - b);
        System.out.println("Subtraction = " + subtraction.operation(5, 3));

        Arithmetic multiplication = (int a, int b) -> (a * b);
        System.out.println("Multiplication = " + multiplication.operation(4, 6));

        Arithmetic division = (int a, int b) -> (a / b);
        System.out.println("Division = " + division.operation(12, 6));

    }
}