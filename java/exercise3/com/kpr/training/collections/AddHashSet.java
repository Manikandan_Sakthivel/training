/*
 * Requirements:
 *    To demonstrate the basic add and traversal operation of linked hash set.
 * 
 * Entities:
 *    AddHashSet 
 *
 * Function Declaration:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1.Create the package collections.
 *    2.Import Iterator,LinkedHashset.
 *    3.Create the class AddHashSet.
 *    4.Create the object set as Generic type.
 *    5.Add the values in set.
 *    6.Print the set elements.
 *    7.Using iterator print the set elements.
 *    */
package com.kpr.training.collections;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class AddHashSet {

	public static void main(String[] args) {
		LinkedHashSet<Integer> set = new LinkedHashSet<>();
		set.add(1);
		set.add(2);
		set.add(3);
		set.add(4);
		set.add(5);
		System.out.println("Set elements are " + set);
		System.out.println("Set elements are using iterator");
		Iterator<Integer> iterator = set.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		System.out.println();
	}
}