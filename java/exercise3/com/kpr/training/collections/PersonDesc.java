/*
* Requirement:
*      Sort the roster list based on the person's age in descending order using java.util.Stream
* Entity:
*      PersonDesc
*      
* Function declaration:
*      public static void main(String[] args)
* 
* Jobs to be done:
*      1) Create package person and import Comparator,List and Stream from util package.
*      2) Create class PersonDesc and Create main method under it.
*      3) Create reference for list named as roster and assign method createRoster for it.
*      4) Compare the age of a person and sort it as descending order.
*      5) Print the person details using the stream.
*/

package com.kpr.training.collections;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonDesc {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = roster.stream().sorted(Comparator.comparing(Person::getAge)).collect(Collectors.toList());
        Stream<Person> stream = newRoster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}