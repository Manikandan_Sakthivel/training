/*
 * Requirement:
 *      Write a program to find the average age of all the Person in the person List
 * Entity:
 *      AverageAge
 * Function Declaration:
 *      public static void main(String[] args)
 * Jobs to be done:
 *      1) Create package person and import List.
 *      2) Create class named as AverageAge
 *      3) Create reference for list named as roster and assign method createRoster for it.
 *      4) Give the condition to find the average age of all the person.
 *      5) Print the average age.
 */
package com.kpr.training.collections;

import java.util.List;

public class AverageAge {
	
    public static void main(String[] args) {
      List<Person> roster = Person.createRoster();
      
      double average = roster
          .stream()
          .mapToInt(Person::getAge)
          .average()
          .getAsDouble();
          
      System.out.println("Average age : " +
          average);


    }

}