/*
 * Requirement:
 *      Print all the persons in the roster using java.util.Stream<T>#forEach 
 * Entity:
 *      public class PrintPerson
 * Function Declaration:
 *      public static void main(String[] args)
 * Jobs to be done:
 *      1) Import List and Stream.
 *      2) Create class named as PrintPerson.
 *      3) Create reference for list named as roster and assign method createRoster for it.
 *      4) Create reference for Stream named as stream and call the method stream.
 *      5) Print each Person using forEach method.
 */
package com.kpr.training.collections;

import java.util.List;
import java.util.stream.Stream;

public class PrintPerson {

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		Stream<Person> stream = roster.stream();
		stream.forEach(person -> System.out.println(person.name + " " +person.birthday + " " +person.gender + " " + person.emailAddress));
	}
}