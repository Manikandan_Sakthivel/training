/*
 * Requirements : 
 * 		Code for sorting vetor list in descending order
 * Entities :
 * 		public class ReverseList.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1)In collections package, importing the arraylist and Collections.
 * 		2)Creating the ReverseList class.
 * 		3)Creating the list object and appending the list values.
 * 	    4)Print the original list.
 *      5)Reverse the list by using the function "reverseOrder() ".
 * 		9)Printing the reversed list.
 */
package com.kpr.training.collections;

import java.util.ArrayList;
import java.util.Collections;

public class ReverseList {
	
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		list.add(60);
		list.add(70);
		System.out.println("Before Reversing : " + list);
		
		Collections.sort(list,Collections.reverseOrder());
		System.out.println("After Reversing :" + list);
	}
}