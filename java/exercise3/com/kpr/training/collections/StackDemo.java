/*
Requirement:
    Add and remove the elements in stack.      
Entity:
    StackDemo

Function Declaration:
    No function is declared

Jobs To Be Done:
    1)Creating the package collections.
    2)Importing the Stack.
    3)Creating the StackDemo class.
    4)Creating the sample object with stack class.
    5)Pushing the 5 elements .
    6)Removing the some of the elements using "remove()" , then print the sample .
*/
package com.kpr.training.collections;

import java.util.Stack;

public class StackDemo  {
	
    public static void main(String[] args) {
        Stack<Integer> sample = new Stack<>();
        sample.push(12);
        sample.push(14);
        sample.push(16);
        sample.push(18);
        sample.push(77);
        System.out.println(sample);
        sample.remove(2);
        sample.remove(1);
        System.out.println("After removing the some of element :" + sample);
    }
    
    
}