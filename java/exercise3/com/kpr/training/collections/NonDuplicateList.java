/*
 * Requirement:
 *       Consider a following code snippet:
 *   List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
 *      - Get the non-duplicate values from the above list using java.util.Stream API
 * Entity:
 *     NonDulicateList
 * Function Declaration:
 *     public static void main(String[] args)
 * Jobs to be done:
 *    1) Create package person,
 *    2) import packages Arrays,List,Collectors and Stream.
 *    3) Create class NonDuplicateList and Create main method in it.
 *    4) Create referece for list named as randomNummbers and assign the given values.
 *    5) Create another list named as  withoutDuplicate and iterate the values from randomNumbers as elements and filter it by using freuency method it returns the value which is Non duplicated and collect it and store it in withoutDuplicate list.
 *    6)By using stream Print the withoutDuplicate list.
 */
package com.kpr.training.collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NonDuplicateList {
	
    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        List<Integer> withoutDuplicate = randomNumbers.stream()
                .filter(element -> Collections.frequency(randomNumbers, element) == 1)
                .collect(Collectors.toList());
        Stream<Integer> stream = withoutDuplicate.stream();
        stream.forEach(elements -> System.out.print(elements + " "));
    }
}