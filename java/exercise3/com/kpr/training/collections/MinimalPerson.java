/*
 * Requirement:
 * 		To print minimal person with name and email address from the Person class using java.util.Stream<T> API by referring Person.java
 * 
 * Entity:
 * 		MinimalPersonMap
 *
 * Function Declaration:
 *		public static <R, T> void main(String[] args)
 *
 * Jobs To Be Done:
 * 		1)Importing the necessary Packages.
 * 		2)Creating the class MinimalPersonMap.
 * 		3)Accessing the Predefined roster list as a name roster.
 * 		4)Using for loop the name and mail address from the roster list is added to the name and mailId list respectively.
 * 		5)Finding the minimal of name
 * 		6) Printing the Minimal Person name and mailId. 
 * 
 */
package com.kpr.training.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinimalPerson {
	
	public static <R, T> void main(String[] args) {
		List<Person> roster = Person.createRoster();
		ArrayList<String> name = new ArrayList<>();
		ArrayList<String> mailId = new ArrayList<>();
		for(Person p : roster) {
			name.add(p.getName());
			mailId.add(p.getEmailAddress());
		}
		String minimalName =Collections.min(name);
		String minimalId =Collections.min(mailId);

		System.out.println("The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
	}

}
