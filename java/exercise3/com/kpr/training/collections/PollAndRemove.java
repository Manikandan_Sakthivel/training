/*
* Requirement:
*      What is the difference between poll() and remove() method of queue interface?
* Entity:
*      PollAndRemove
* Function declaration:
*      public static void main(String[] args)
* Jobs to be done:
*      *Explain the differenece between poll and remove method.
*      
* Solution:
* pop():
*      ->This method is available in java.util package.
*      ->This method is used to retrieve the head element of the Queue or in other words, it is used to   * retrieve the first element or initial element of the queue.
*      ->In the case of poll() method, it retrieves the head element of the queue and then removes the 
*  head element of the queue.
*      ->In case of poll() method if the queue is empty then it will return null but it does not throw an exception.
*     -> The return type of this method is not void that means this method return first element of the Queue.
*  
*remove():
*      ->This method is available in java.util package.
*      ->This method is used to remove the head element of the Queue and retrieve the first element of * the Queue like poll() method.
*      ->In the case of remove() method, it retrieves the head element and removes the first element of * the Queue as well by calling remove() method.
*      ->In case of remove() method, if Queue is empty then, in that case, it throws an exception 
* NoSuchElementFoundException but it does not return null like of poll() method.
*

*/
package com.kpr.training.collections;

import java.util.PriorityQueue;
import java.util.Queue;

public class PollAndRemove {
    public static void main(String[] args) {
        
        Queue<Integer> queue = new PriorityQueue<>();
        queue.add(100);
        queue.add(200);
        queue.add(300);
        queue.add(400);
        System.out.println("Before using poll :" + queue);
        queue.poll();
        System.out.println("After using poll :" + queue);
        queue.clear();
        System.out.println(queue.poll());
        
        Queue<Integer> queue1 = new PriorityQueue<>();
        queue1.add(10);
        queue1.add(20);
        queue1.add(30);
        queue1.add(40);
        System.out.println("Before using remove :" + queue1);
        queue1.remove();
        System.out.println("After using remove :" + queue1);
        queue1.clear();
        System.out.println(queue.remove()); //throws exception
    }
}