package com.kpr.training.jdbc.exceptions;

public class AppException extends RuntimeException {

	private int errorCode;
	private String errorMsg;

	public AppException(ExceptionCode code) {
		this.errorMsg = code.getMsg();
		this.errorCode = code.getId();
		System.out.println(
				"Error Code " + AppException.this.errorCode + " " + AppException.this.errorMsg);
	}

}
