package com.kpr.training.jdbc.exceptions;

public interface ErrorCode {
	int CODE_001 = 001;
	int CODE_002 = 002;
	int CODE_003 = 003;
}
