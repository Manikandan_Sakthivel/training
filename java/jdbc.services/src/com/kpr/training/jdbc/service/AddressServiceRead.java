/*
 * Requirement:
 *  To read a data in the address .
 *  
 * Entity:
 *  AddressServiceRead
 *  
 * Function Signature :
 *  public static void main(String[] args)
 *  
 * Jobs to be Done:
 *   1. Create a String with name street and set the value .
       1.1) Create a String with name city and set the value .
       1.2) Create a int with name postal_code and set the Default value.
    2. Establish the connection using jdbc driver from createConnection method in jc.
    3. Prepare read query and store it in query of type String.
    4. Inside try block 
       4.1) Create a prepareStatement for the query using jc.con as jc.ps.
       4.2) Create instance for ResultSet class as result
       4.3) Using loop read specific id  values like street,city,postal_code ,id
            and store it in result 
    5. If any Error causes catch block catch the error 
    6. Using loop print the readed values
    7. Close the jdbc connection.
 *  
 * Pseudo code:
 * public class AddressServiceRead {
    
    public static void read(long id) {
        String street = null;
        String city = null;
        int postal_code = 0;
        
        JdbcConnection jc = new JdbcConnection();
        jc.createConnection();
        //set the query.
        try {
            jc.ps = jc.con.prepareStatement(query);
            jc.ps.setLong(1, id);
            ResultSet result = jc.ps.executeQuery();
            while (result.next()) {
                street= result.getString("street");
                city = result.getString("city");
                postal_code = result.getInt("postal_code");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(id + " " + street + " " + city + " " + postal_code);
        jc.closeConnection();
    }
}
 */

package com.kpr.training.jdbc.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.kpr.training.jdbc.exceptions.AppException;
import com.kpr.training.jdbc.exceptions.ExceptionCode;

public class AddressServiceRead {

	public static void read(long id) {
		String street = null;
		String city = null;
		int postal_code = 0;

		JdbcConnection jc = new JdbcConnection();
		jc.createConnection();
		String readQuery =
				"SELECT address.id, address.street, address.city, address.postal_code FROM  `jdbc.services`.address WHERE `id` = (?)";
		try {
			jc.ps = jc.con.prepareStatement(readQuery);
			jc.ps.setLong(1, id);
			ResultSet result = jc.ps.executeQuery();
			while (result.next()) {
				street = result.getString("street");
				city = result.getString("city");
				postal_code = result.getInt("postal_code");
			}
		} catch (SQLException e) {
			throw new AppException(ExceptionCode.SQLException);
		}
		System.out.println(id + " " + street + " " + city + " " + postal_code);
		jc.closeConnection();
	}
}
