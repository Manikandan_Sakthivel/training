/*
Requirement:
    To delete the record of a particular id.
    
Entity:
    AddressServiceDelete
    
Function declaration:
    public static void delete(long id)
    
Jobs to be done:
    1. Create an instance of the class JdbcConnection as jc.
    2. Establish the connection using jdbc driver from createConnection method.
    3. Prepare delete query and store in query as type of String.
    4. Create a prepareStatement of the query using jc.con as jc.ps.
    5. Set the id to delete its record.
    6. Execute jc.ps
    7. Close the connection established.
    8. Print the message as success.

Pseudo code:
class AddressServiceDelete {

    public static void delete(long id) {
        JdbcConnection jc = new JdbcConnection();
        jc.createConnection();
        String deleteQuery = "delete from  `jdbc`.address " + "where id=?";
        try {
            jc.ps = jc.con.prepareStatement(deleteQuery);
            jc.ps.setLong(1, id);
            jc.ps.executeUpdate();
        } catch (SQLException e) {
            throw new AppException(ExceptionCode.SQLException);
        }
        jc.closeConnection();
    }
    
}
*/

package com.kpr.training.jdbc.service;

import java.sql.SQLException;
import com.kpr.training.jdbc.exceptions.AppException;
import com.kpr.training.jdbc.exceptions.ExceptionCode;

public class AddressServiceDelete {

	public static void delete(long id) {
		JdbcConnection jc = new JdbcConnection();
		jc.createConnection();
		String deleteQuery = "DELETE FROM `jdbc.services`.address WHERE id=?";
		try {
			jc.ps = jc.con.prepareStatement(deleteQuery);
			jc.ps.setLong(1, id);
			jc.ps.executeUpdate();
		} catch (SQLException e) {
			throw new AppException(ExceptionCode.SQLException);
		}
		jc.closeConnection();
	}

}
