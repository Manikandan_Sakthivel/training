/*
Requirement:
    To create a conection between the database and java application.
    
Entity:
    JdbcConnection
    
Function declaration:
    public void createConnection() {...}
    public void closeConnection() {...}
    
Jobs to be done:
    1. Assign the server link in url of type String.
    2. Assign the user name in user of type String.
    3. Assign the database password in password of type String.
    4. Declare Connection and PreparedStatement as con and ps.
    5. Declare createConnection method.
        5.1 Establish the connection using sql driver and store it as con.
    6. Declare closeConnecton method.
        6.1 Close the Connection of the server.
        6.2 Close the PreparedStatement of the query.

Pseudo code:
class JdbcConnection {
    
    String url = "jdbc:mysql://localhost:3306/?user=root?autoReconnect=true&useSSL=false";
    String user = "root";
    String password = "kiruthic05";
    
    Connection con;
    PreparedStatement ps;
    

    public void createConnection() {
        
        try {
            con = DriverManager.getConnection(url, user, password); 
            
            
        } catch (SQLException e) {
        	throw new AppException(ExceptionCode.SQLException);
        }

    }
    
    public void closeConnection() {
        try {
            con.close();
            ps.close();
        } catch (SQLException e) {
        	throw new AppException(ExceptionCode.SQLException);
        }
    }
}
*/

package com.kpr.training.jdbc.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.kpr.training.jdbc.exceptions.AppException;
import com.kpr.training.jdbc.exceptions.ExceptionCode;

public class JdbcConnection {

	private String url = "jdbc:mysql://localhost:3306/?user=root?autoReconnect=true&useSSL=false";
	private String user = "root";
	private String password = "Mani@8301";

	Connection con;
	PreparedStatement ps;


	public void createConnection() {

		try {
			con = DriverManager.getConnection(url, user, password);


		} catch (SQLException e) {
			throw new AppException(ExceptionCode.SQLException);
		}

	}

	public void closeConnection() {
		try {
			con.close();
			ps.close();
		} catch (SQLException e) {
			throw new AppException(ExceptionCode.SQLException);
		}
	}
}
