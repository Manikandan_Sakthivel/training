/*
 * Requirement:
 * 		To perform the CRUD operation of the address.
 * 
 * Entity:
 * 		AddressService
 * 
 * Method Signature:
 * 		public static void create(Address address) {}
 * 		public static void read(long id) {}
 * 		public static void readAll() {}
 * 		public static void update(long id, Address address) {}
 * 		public static void delete(long id) {}
 * 
 * Jobs To Be Done:
 * 		1)To Perform the CRUD operation of the address service, the individual operations should be done.
 * 		2)Create each method to call the individual operation. 
 * '		2.1) 'create' method is called in AddressServiceCreate with address as parameter.
 * 			2.2) 'read' method is called in AddressServiceRead with reference to id.
 * 			2.3) 'readAll' method is called in AddressServiceRealAll to read all the values.
 * 			2.4) 'update' method is called in AddressServiceUpdate to update the particular value.
 * 			2.5) 'delete' method is called in AddressServiceDelete to delete the particular value.
 * 
 * Pseudo Code:
 * 		class AddressService {
 * 			public static void create(Address address) {
				AddressServiceCreate.create(address);
				
			}
			
			public static void read(long id) {
				AddressServiceRead.read(id);
			}
			
			public static void readAll() {
				AddressServiceReadAll.readAll();
			}
			
			public static void update(long id, Address address) {
				AddressServiceUpdate.update(id, address);
			}
			
			public static void delete(long id) {
				AddressServiceDelete.delete(id);
			}
 * 		}
 * 		
 */
package com.kpr.training.jdbc.service;

import com.kpr.training.jdbc.model.Address;

public class AddressService {
	
	public static void create(Address address) {
		AddressServiceCreate.create(address);
		
	}
	
	public static void read(long id) {
		AddressServiceRead.read(id);
	}
	
	public static void readAll() {
		AddressServiceReadAll.readAll();
	}
	
	public static void update(long id, Address address) {
		AddressServiceUpdate.update(id, address);
	}
	
	public static void delete(long id) {
		AddressServiceDelete.delete(id);
	}
}
