package com.kpr.training.jdbc.constant;

public class Query {

	String createQuery =
			"INSERT INTO `jdbc.services`.address (street, city, postal_code) VALUES (?, ?, ?)";
	String deleteQuery = "DELETE FROM `jdbc.services`.address WHERE id=?";
	String readQuery =
			"SELECT address.id, address.street, address.city, address.postal_code FROM  `jdbc.services`.address WHERE `id` = (?)";
	String readAllQuery =
			"SELECT address.id, address.street, address.city, address.postal_code FROM  `jdbc.services`.address";
	String updateQuery =
			"UPDATE `jdbc.services`.address SET street = ?, city = ?, postal_code = ? WHERE id = ?";
}
