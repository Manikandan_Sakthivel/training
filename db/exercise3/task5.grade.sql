  SELECT university.university_name
        ,student.id as roll_number
        ,student.name as student_name
        ,student.gender
        ,student.dob
        ,student.address
        ,college.name AS college_name
        ,department.dept_name
        ,semester_result.semester
        ,semester_result.grade
        ,semester_result.credits
    FROM university 
        ,college 
        ,department  
        ,designation 
        ,college_department 
        ,employee 
        ,student 
        ,syllabus 
        ,professor_syllabus 
        ,semester_result 
 WHERE college.univ_code = university.univ_code 
   AND university.univ_code = department.univ_code 
   AND college_department.college_id = college.id 
   AND college_department.udept_code = department.dept_code
   AND employee.college_id = college.id 
   AND employee.cdept_id = college_department.cdept_id 
   AND employee.desig_id = designation.id 
   AND student.college_id = college.id
   AND student.cdept_id = college_department.cdept_id
   AND syllabus.cdept_id = college_department.cdept_id
   AND professor_syllabus.syllabus_id = syllabus.id
   AND semester_result.syllabus_id = syllabus.id
   AND semester_result.stud_id = student.id
 ORDER BY semester ;