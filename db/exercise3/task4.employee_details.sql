SELECT university.univ_code
      ,college.name
      ,university.university_name
      ,college.city
      ,college.state
      ,college.year_opened
      ,department.dept_name
      ,employee.name employee_name
      ,designation.name as designation
      ,designation.rank as employee_rank
  FROM university 
      ,college 
      ,department 
      ,designation 
      ,college_department 
      ,employee  
 WHERE college.univ_code = university.univ_code 
   AND university.univ_code = department.univ_code 
   AND college_department.college_id = college.id 
   AND college_department.udept_code = department.dept_code
   AND employee.college_id = college.id 
   AND employee.cdept_id = college_department.cdept_id 
   AND employee.desig_id = designation.id 
   AND university.univ_code = 'AU01'
 ORDER BY designation.rank;