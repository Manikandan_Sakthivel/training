SELECT first_name
      ,surname
  FROM employeedetails.employee_table
 WHERE MONTH(dob) = MONTH(NOW()) 
   AND DAY(dob) = DAY(NOW())
 