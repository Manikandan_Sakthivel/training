SELECT department_name
	  ,MIN(employee_table.annual_salary) AS least_paid
      ,MAX(employee_table.annual_salary) AS highest_paid
  FROM employeedetails.employee_table
      ,employeedetails.department_table
 WHERE employee_table.department_id = department_table.department_id
 GROUP BY department_table.department_id