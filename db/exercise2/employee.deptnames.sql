SELECT employee.first_name
      ,employee.surname
      ,department.department_name
  FROM employeedetails.employee_table employee
      ,employeedetails.department_table department
 WHERE employee.department_id = department.department_id