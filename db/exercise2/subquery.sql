SELECT emp_id
      ,first_name
      ,dob
      ,annual_salary
  FROM employeedetails.employee_table
 WHERE department_id =
      ( SELECT department_id
          FROM employeedetails.department_table
	     WHERE department_id = 1);
      