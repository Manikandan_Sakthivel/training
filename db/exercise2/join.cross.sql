CREATE VIEW newview AS
SELECT employee_table.emp_id
	  ,employee_table.first_name
      ,employee_table.surname
      ,department_table.department_name
  FROM employeedetails.employee_table
CROSS JOIN employeedetails.department_table;

SHOW TABLES;

SELECT *
  FROM newview;
