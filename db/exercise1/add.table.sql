CREATE TABLE school.student_marklist (
             roll_no INT AUTO_INCREMENT PRIMARY KEY
			,student_name VARCHAR(45)
            ,typeof_exam VARCHAR(45)
			,english INT
            ,tamil INT
            ,maths INT
            ,science INT
            ,social INT
            ,total_marks INT
            ,secured_rank VARCHAR(45) DEFAULT 'no rank'
            ,grade VARCHAR(45)
            ,classteacher_name VARCHAR(45)
            )

            