CREATE TABLE school.teacher(
             teacher_id INT
			,teacher_name VARCHAR(45)
            ,monthly_salary INT
            ,major_subject VARCHAR(45)
            ,address VARCHAR(45)
            )